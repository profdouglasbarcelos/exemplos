﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WingTipToys.BLL;
using WingTipToys.Models;

namespace WingTipToys
{
    public partial class CarrinhoCompra : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            using(AcoesCarrinhoCompraBLL acoes = new AcoesCarrinhoCompraBLL())
            {
                decimal total = 0;
                total = acoes.RecuperarTotal();

                if(total > 0)
                {
                    lblTotal.Text = String.Format("{0:c}", total);
                }
                else
                {
                    LabelTotalText.Text = String.Empty;
                    lblTotal.Text = String.Empty;
                    ShoppingCartTitle.InnerText = "Carrinho de compra vazio";
                }
            }
        }

        public List<ItemCarrinho> ListarItensCarrinhoCompra()
        {
            AcoesCarrinhoCompraBLL acoes = new AcoesCarrinhoCompraBLL();
            return acoes.ListarItensCarrinhoCompra();
        }

        protected void ListaCarrinho_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            string comando = e.CommandName;
            string argumento = e.CommandArgument.ToString();


        }
    }
}