﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WingTipToys.BLL;

namespace WingTipToys
{
    public partial class AdicionarAoCarrinho : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string strIdProduto = Request.QueryString["produtoID"];

            int idProduto;

            if(!String.IsNullOrEmpty(strIdProduto) && 
                Int32.TryParse(strIdProduto, out idProduto))
            {
                using (AcoesCarrinhoCompraBLL acoes = new AcoesCarrinhoCompraBLL())
                {
                    acoes.AdicionarAoCarrinho(idProduto);
                }
            }
            else
            {
                Debug.Fail("Id Produto não informado!");
                throw new Exception("Id Produto não informado!");
            }

            Response.Redirect("CarrinhoCompra.aspx");
        }
    }
}