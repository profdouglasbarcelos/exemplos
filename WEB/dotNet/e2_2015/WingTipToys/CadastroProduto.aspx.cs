﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WingTipToys.BLL;
using WingTipToys.Models;

namespace WingTipToys
{
    public partial class CadastroProduto : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CarregarDDLCategoria();
                CarregarProduto();
            }
        }

        private void CarregarProduto()
        {
            String strProduto = Request.QueryString["idProduto"];

            int idProd;

            if(!String.IsNullOrEmpty(strProduto) && Int32.TryParse(strProduto, out idProd))
            {
                if(idProd != 0)
                {
                    Produto prod = ProdutoBLL.CarregarProdutoPorId(idProd);
                    txtNome.Text = prod.NomeProduto;
                    txtDescricao.Text = prod.Descricao;
                    txtValor.Text = prod.Valor.ToString();
                    ddlCategoria.SelectedValue = prod.CategoriaID.ToString();
                    btnAdicionar.Text = "Salvar";
                }
            }
        }

        private void CarregarDDLCategoria()
        {
            ddlCategoria.DataValueField = "CategoriaID";
            ddlCategoria.DataTextField = "CategoriaNome";

            ddlCategoria.DataSource = CategoriaBLL.RetornarTodasCategorias();
            ddlCategoria.DataBind();
        }

        protected void btnCancelar_Click(object sender, EventArgs e)
        {
            txtNome.Text = String.Empty;
            txtDescricao.Text = String.Empty;
            //ddlCategoria
            txtValor.Text = String.Empty;
        }

        protected void btnAdicionar_Click(object sender, EventArgs e)
        {
            Produto prod = new Produto();

            String strProduto = Request.QueryString["idProduto"];
            int idProd;

            if (!String.IsNullOrEmpty(strProduto) && Int32.TryParse(strProduto, out idProd))
            {
                if (idProd != 0)
                {
                    prod.ProdutoID = idProd;
                }
            }


            prod.NomeProduto = txtNome.Text;
            prod.CategoriaID = Convert.ToInt32(ddlCategoria.SelectedValue);
            prod.Descricao = txtDescricao.Text;
            prod.Valor = Convert.ToDouble(txtValor.Text);
            prod.CaminhoImagem = "rocket.png";

            if(ProdutoBLL.CadastrarProduto(prod))
            {
                lblNome.Text = "Cadastrado com sucesso!";
                lblNome.CssClass = "text-success";
            }
            else
            {
                lblNome.Text = "Falha ao cadastrar!";
                lblNome.CssClass = "text-danger";
            }
        }
    }
}