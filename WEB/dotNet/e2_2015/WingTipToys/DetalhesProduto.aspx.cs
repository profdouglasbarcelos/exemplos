﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.ModelBinding;
using System.Web.UI;
using System.Web.UI.WebControls;
using WingTipToys.BLL;
using WingTipToys.Models;

namespace WingTipToys
{
    public partial class DetalhesProduto : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        //public IQueryable<Produto> CarregarProduto([QueryString("produtoID")] int? idProduto)
        //{
        //    using (var contexto = new WingTipToys.Models.ProdutoContexto())
        //    {
        //        IQueryable<Produto> lista = null;

        //        if (idProduto.HasValue && idProduto > 0)
        //        {
        //            lista = contexto.Produtos.AsNoTracking();
        //            lista = lista.Where(p => p.ProdutoID == idProduto);
        //        }

        //        return lista;
        //    }
        //}

        public IList<Produto> CarregarProduto([QueryString("produtoID")] int? idProduto)
        {
            using (var contexto = new WingTipToys.Models.ProdutoContexto())
            {
                IQueryable<Produto> lista = null;

                if (idProduto.HasValue && idProduto > 0)
                {
                    lista = contexto.Produtos.AsNoTracking();
                    lista = lista.Where(p => p.ProdutoID == idProduto);

                    string acao = Request.QueryString["acao"];

                    if(!String.IsNullOrEmpty(acao) && acao.Equals("Excluir"))
                    {
                        lblConfirmarExclusao.Visible = true;
                        btnExcluir.Visible = true;
                        ViewState["idProd"] = idProduto.ToString();
                    }
                }

                return lista.ToList<Produto>();
            }
        }

        protected void btnExcluir_Click(object sender, EventArgs e)
        {
            String strIdProd = ViewState["idProd"].ToString();
            int idProd;
            
            if(!String.IsNullOrEmpty(strIdProd) && Int32.TryParse(strIdProd, out idProd))
            {
                if(idProd > 0)
                {
                    ProdutoBLL.ExcluirProduto(idProd);
                }
            }
        }
    }
}