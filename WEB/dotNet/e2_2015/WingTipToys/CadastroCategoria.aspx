﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="CadastroCategoria.aspx.cs" Inherits="WingTipToys.CadastroCategoria" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <h1>Cadastro de Categoria</h1>

    

    <table style="width:100%;">
        <tr>
            <td class="modal-sm" style="width: 119px">
                <asp:Label ID="lblNome" runat="server" Text="Nome"></asp:Label>
            </td>
            <td>
                <asp:RequiredFieldValidator ID="rfvNome" CssClass="text-danger" runat="server" ErrorMessage="O campo nome é obrigatório!" ControlToValidate="txtNome"></asp:RequiredFieldValidator>
                <asp:TextBox ID="txtNome" CssClass="form-control" runat="server"></asp:TextBox>
                <asp:CustomValidator ID="cvTamanho" runat="server" ErrorMessage="Tamanho inválido!" CssClass="text-danger" ControlToValidate="txtNome" ClientValidationFunction="tamanhoMaximoString"></asp:CustomValidator>
            </td>
            
        </tr>
        <tr>
            <td class="modal-sm" style="width: 119px">
                <asp:Label ID="lblDescricao" runat="server" Text="Descrição"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtDescricao" TextMode="MultiLine" Rows="5" CssClass="form-control" runat="server"></asp:TextBox>
            </td>
            
        </tr>
        <tr>
            <td class="modal-sm" style="width: 119px">&nbsp;</td>
            <td>&nbsp;</td>
            
        </tr>
        <tr>
            <td class="modal-sm" style="width: 119px">
                <asp:Button ID="btnCadastrar" CssClass="btn btn-primary" runat="server" Text="Cadastrar" OnClick="btnCadastrar_Click" />
            </td>
            <td>
                <asp:Button ID="btnCancelar" CssClass="btn btn-default" runat="server" Text="Cancelar" CausesValidation="false" OnClick="btnCancelar_Click" />
            </td>
            
        </tr>
    </table>
    
    <script type="text/javascript" language="javascript">
        function tamanhoMaximoString(source, arguments) {
            if (arguments.Value.length > 10) {
                arguments.IsValid = false;
            } else {
                arguments.IsValid = true;
            }
        }
        </script>
</asp:Content>
