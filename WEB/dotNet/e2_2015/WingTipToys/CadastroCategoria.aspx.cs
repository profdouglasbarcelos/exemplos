﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WingTipToys.BLL;
using WingTipToys.Models;

namespace WingTipToys
{
    public partial class CadastroCategoria : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnCadastrar_Click(object sender, EventArgs e)
        {
            //if (!String.IsNullOrEmpty(txtNome.Text))
            Page.Validate();
            if(Page.IsValid)
            {
                Categoria categ = new Categoria();

                categ.CategoriaNome = txtNome.Text;
                categ.Descricao = txtDescricao.Text;

                CategoriaBLL.CadastrarCategoria(categ);
            }
        }

        protected void btnCancelar_Click(object sender, EventArgs e)
        {
            LimparFormulario();
        }

        private void LimparFormulario()
        {
            txtNome.Text = String.Empty;
            txtDescricao.Text = String.Empty;
        }
    }
}