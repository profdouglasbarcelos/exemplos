﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="CarrinhoCompra.aspx.cs" Inherits="WingTipToys.CarrinhoCompra" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    
    <div id="ShoppingCartTitle" runat="server" class="ContentHead"><h1>Carrinho Compras</h1></div>
    
    <asp:GridView ID="ListaCarrinho" runat="server" AutoGenerateColumns="False" ShowFooter="True" GridLines="Vertical" CellPadding="4"
        ItemType="WingTipToys.Models.ItemCarrinho" SelectMethod="ListarItensCarrinhoCompra" 
        CssClass="table table-striped table-bordered" OnRowCommand="ListaCarrinho_RowCommand" >   
        <Columns>
        <asp:BoundField DataField="ProdutoID" HeaderText="Código Produto" SortExpression="ProdutoID" />        
        <asp:BoundField DataField="Produto.NomeProduto" HeaderText="Nome" />        
        <asp:BoundField DataField="Produto.Valor" HeaderText="Preço Unitário" DataFormatString="{0:c}"/>     
        <asp:TemplateField   HeaderText="Quantidade">            
                <ItemTemplate>
                    <asp:TextBox ID="txtQuantidadeComprada" Width="40" runat="server" 
                        Text="<%#: Item.Quantidade %>"></asp:TextBox> 
                </ItemTemplate>        
        </asp:TemplateField>    
        <asp:TemplateField HeaderText="Valor Total">            
                <ItemTemplate>
                    <%#: String.Format("{0:c}", ((Convert.ToDouble(Item.Quantidade)) *  Convert.ToDouble(Item.Produto.Valor)))%>
                </ItemTemplate>        
        </asp:TemplateField> 
        <asp:TemplateField HeaderText="Excluir">            
                <ItemTemplate>
                    <asp:CheckBox id="cbxExcluir" runat="server"></asp:CheckBox>
                    <asp:Button ID="btnExcluir" runat="server" Text="Excluir" CommandName="Excluir" CommandArgument="<%#: Item.ProdutoID %>" />
                </ItemTemplate>        
        </asp:TemplateField>    
        </Columns>    
    </asp:GridView>
    <div>
        <p></p>
        <strong>
            <asp:Label ID="LabelTotalText" runat="server" Text="Total: "></asp:Label>
            <asp:Label ID="lblTotal" runat="server" EnableViewState="false"></asp:Label>
        </strong> 
    </div>
    <br />

</asp:Content>
