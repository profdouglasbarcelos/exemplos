﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WingTipToys.Models
{
    public class ItemCarrinho
    {
        [Key]
        public string ItemID { get; set; }

        public string CarrinhoID { get; set; }
        public int Quantidade { get; set; }
        public DateTime DataCriacao { get; set; }
        public int ProdutoID { get; set; }
        public virtual Produto Produto { get; set; }
    }
}