﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WingTipToys.Models
{
    public class Produto
    {
        [ScaffoldColumn(false)]
        public int ProdutoID { get; set; }

        [Required]
        [StringLength(100)]
        [Display(Name="Nome")]
        public string NomeProduto { get; set; }
        
        [Required, StringLength(10000), 
        Display(Name="Descrição do Produto"), 
        DataType(DataType.MultilineText)]
        public string Descricao { get; set; }
        
        [Display(Name="Preço")]
        public double? Valor { get; set; }

        public string Marca { get; set; }
        public string Fornecedor { get; set; }
        
        public string CaminhoImagem { get; set; }

        public int? CategoriaID { get; set; }

        public virtual Categoria Categoria { get; set; }
    }
}