﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WingTipToys.Models
{
    public class Categoria
    {
        public enum TipoCategoria
        {
            Carros = 1,
            Avioes = 2,
            Caminhoes = 3,
            Embarcacoes = 4,
            Foguetes = 5
        }

        [ScaffoldColumn(false)]
        public int CategoriaID { get; set; }

        [Required, StringLength(100), Display(Name="Nome")]
        public string CategoriaNome { get; set; }
        
        [Display(Name="Descrição")]
        public string Descricao { get; set; }

        public virtual ICollection<Produto> Produtos { get; set; }

    }
}