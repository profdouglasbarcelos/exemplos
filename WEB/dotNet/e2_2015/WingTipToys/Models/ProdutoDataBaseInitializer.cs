﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace WingTipToys.Models
{
    public class ProdutoDataBaseInitializer : DropCreateDatabaseIfModelChanges<ProdutoContexto>
    {
        protected override void Seed(ProdutoContexto context)
        {
            //base.Seed(context);
            BuscarCategorias().ForEach(categoria => context.Categorias.Add(categoria));

            //List<Categoria> listaCateg = new List<Categoria>();
            //listaCateg = BuscarCategorias();

            //foreach (Categoria categ in listaCateg)
            //{
            //    context.Categorias.Add(categ);
            //}

            BuscarProdutos().ForEach(produto => context.Produtos.Add(produto));         

        }

        private static List<Categoria> BuscarCategorias()
        {
            var categorias = new List<Categoria>
            {
                new Categoria
                {
                    CategoriaID = (int)Categoria.TipoCategoria.Carros,
                    CategoriaNome = "Carros"
                },

                new Categoria
                {
                    CategoriaID = (int)Categoria.TipoCategoria.Avioes,
                    CategoriaNome = "Aviões"
                },

                new Categoria
                {
                    CategoriaID = (int)Categoria.TipoCategoria.Caminhoes,
                    CategoriaNome = "Caminhões"
                },

                new Categoria
                {
                    CategoriaID = (int)Categoria.TipoCategoria.Embarcacoes,
                    CategoriaNome = "Embarcações"
                },

                new Categoria
                {
                    CategoriaID = (int)Categoria.TipoCategoria.Foguetes,
                    CategoriaNome = "Foguetes"
                }
            };

            return categorias;
        }

        private static List<Produto> BuscarProdutos()
        {
            var produtos = new List<Produto>
            {
                new Produto
                {
                    ProdutoID = 1,
                    NomeProduto = "Carro Conversível",
                    Descricao = "Este carro conversível é rápido.",
                    CaminhoImagem = "carconvert.png",
                    Valor = 150.50,
                    CategoriaID = 1,
                    Marca = "Estrela",
                    Fornecedor = "Estrela"
                },

                new Produto
                {
                    ProdutoID = 2,
                    NomeProduto = "Carro Antigo",
                    Descricao = "Não há nada de velho neste carro, apenas o visual.",
                    CaminhoImagem = "carearly.png",
                    Valor = 250.50,
                    CategoriaID = 1,
                    Marca = "Matel",
                    Fornecedor = "Matel Curitiba"
                },

                new Produto
                {
                    ProdutoID = 3,
                    NomeProduto = "Carro super rápido",
                    Descricao = "Carro hibrido, super rápido.",
                    CaminhoImagem = "carfast.png",
                    Valor = 600.50,
                    CategoriaID = 1,
                    Marca = "Ben 10",
                    Fornecedor = "Cartoon Network"
                },

                new Produto
                {
                    ProdutoID = 4,
                    NomeProduto = "Avião Caça",
                    Descricao = "Avião de brinquedo autêntico. Características realistas.",
                    CaminhoImagem = "planeace.png",
                    Valor = 200.50,
                    CategoriaID = 2,
                    Marca = "Estrela",
                    Fornecedor = "Estrela"
                },

                new Produto
                {
                    ProdutoID = 5,
                    NomeProduto = "Planador",
                    Descricao = "Planador divertido.",
                    CaminhoImagem = "planeglider.png",
                    Valor = 10.50,
                    CategoriaID = 2,
                    Marca = "Isopor",
                    Fornecedor = "Tio do parque"
                },

                new Produto
                {
                    ProdutoID = 6,
                    NomeProduto = "Avião de papel",
                    Descricao = "Não há outro avião como este avião de papel.",
                    CaminhoImagem = "planepaper.png",
                    Valor = 0.50,
                    CategoriaID = 2,
                    Marca = "Chamequinho",
                    Fornecedor = "Cocelpa"
                },

                new Produto
                {
                    ProdutoID = 7,
                    NomeProduto = "Caminhão Antigo",
                    Descricao = "Versão Cobra.",
                    CaminhoImagem = "truckearly.png",
                    Valor = 350.50,
                    CategoriaID = 3,
                    Marca = "Scania",
                    Fornecedor = "Scania"
                },

                new Produto
                {
                    ProdutoID = 8,
                    NomeProduto = "Big Foot",
                    Descricao = "Use esta caminhão para esmagar os demais.",
                    CaminhoImagem = "truckbig.png",
                    Valor = 450.50,
                    CategoriaID = 3,
                    Marca = "Demolidor",
                    Fornecedor = "Stalone Cobra"
                },

                new Produto
                {
                    ProdutoID = 9,
                    NomeProduto = "Navio Cruzeiro",
                    Descricao = "Cruzeiro das praias do Brasil.",
                    CaminhoImagem = "boatbig.png",
                    Valor = 750.50,
                    CategoriaID = 4,
                    Marca = "Titanic",
                    Fornecedor = "Jack & Rose"
                },

                new Produto
                {
                    ProdutoID = 10,
                    NomeProduto = "Barco de papel",
                    Descricao = "Clássico da diversão. Multiuso",
                    CaminhoImagem = "boatpaper.png",
                    Valor = 1.50,
                    CategoriaID = 4,
                    Marca = "Chamequinho",
                    Fornecedor = "Soldadinho de chumbo"
                },

                new Produto
                {
                    ProdutoID = 11,
                    NomeProduto = "Foguete",
                    Descricao = "Ao infinito e além.",
                    CaminhoImagem = "rocket.png",
                    Valor = 950.50,
                    CategoriaID = 5,
                    Marca = "Houston",
                    Fornecedor = "Nasa"
                },
            };

            return produtos;
        }
    }
}