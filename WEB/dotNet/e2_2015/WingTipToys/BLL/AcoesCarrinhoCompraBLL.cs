﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WingTipToys.Models;

namespace WingTipToys.BLL
{
    public class AcoesCarrinhoCompraBLL : IDisposable
    {
        public string CarrinhoCompraID { get; set; }

        private ProdutoContexto contexto = new ProdutoContexto();

        private const string chaveSessaoCarrinho = "CarrinhoID";

         public void AdicionarAoCarrinho(int produtoID)
        {
            // Retrieve the product from the database.           
            CarrinhoCompraID = BuscarIdCarrinhoCompra();

            var itemCarrinho = contexto.ItensCarrinhoCompra.SingleOrDefault(
                carrinho => carrinho.CarrinhoID == CarrinhoCompraID
                && carrinho.ProdutoID == produtoID);

             if(itemCarrinho == null)
             {
                 // nao exite este item no carrinho (adicionando pela primeira vez)
                 itemCarrinho = new ItemCarrinho
                 {
                     ItemID = Guid.NewGuid().ToString(),
                     ProdutoID = produtoID,
                     CarrinhoID = CarrinhoCompraID,
                     Produto = contexto.Produtos.SingleOrDefault(
                     prod => prod.ProdutoID == produtoID),
                     Quantidade = 1,
                     DataCriacao = DateTime.Now
                 };

                 contexto.ItensCarrinhoCompra.Add(itemCarrinho);
             }
            else
            {
                itemCarrinho.Quantidade++;
            }
            
             contexto.SaveChanges();
        }

         public string BuscarIdCarrinhoCompra()
         {
             if(HttpContext.Current.Session[chaveSessaoCarrinho] == null)
             {
                 if (!string.IsNullOrWhiteSpace(HttpContext.Current.User.Identity.Name))
                 {
                     HttpContext.Current.Session[chaveSessaoCarrinho] = 
                         HttpContext.Current.User.Identity.Name;
                 }
                 else
                 {
                     Guid tempCartId = Guid.NewGuid();
                     HttpContext.Current.Session[chaveSessaoCarrinho] = 
                         tempCartId.ToString();
                 }
             }

             return HttpContext.Current.Session[chaveSessaoCarrinho].ToString();
         }

        public void Dispose()
         {
            if(contexto != null)
            {
                contexto.Dispose();
                contexto = null;
            }
         }

        public List<ItemCarrinho> ListarItensCarrinhoCompra()
        {
            CarrinhoCompraID = BuscarIdCarrinhoCompra();

            return contexto.ItensCarrinhoCompra.Where(
                carrinho => carrinho.CarrinhoID == CarrinhoCompraID).ToList();
        }

        public decimal RecuperarTotal()
        {
            CarrinhoCompraID = BuscarIdCarrinhoCompra();

            decimal? total = decimal.Zero;

            //foreach (var item in contexto.ItensCarrinhoCompra)
            //{
            //    if(item.CarrinhoID == CarrinhoCompraID)
            //    {
            //        total += (decimal?)(item.Quantidade * item.Produto.Valor);
            //    }
            //}

            total = (decimal?)(from itens in contexto.ItensCarrinhoCompra
                               where itens.CarrinhoID == CarrinhoCompraID
                               select (int?)itens.Quantidade *
                               itens.Produto.Valor).Sum();          
            
            //Exemplo de operador ternario
            //bool x = total.HasValue ? true : false;

            //if (total.HasValue)
            //    return total.Value;
            //else
            //    return decimal.Zero;

            return total ?? decimal.Zero;
        }
        
    }
}