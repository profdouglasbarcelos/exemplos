﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;
using WingTipToys.Models;

namespace WingTipToys.BLL
{
    public class ProdutoBLL
    {
        public static bool CadastrarProduto(Produto prod)
        {
            using (var contexto = new ProdutoContexto())
            {
                if (prod.ProdutoID > 0)
                {
                    Produto produtoAntigo = contexto.Produtos.Where(pr => pr.ProdutoID == prod.ProdutoID).FirstOrDefault();
                    
                    produtoAntigo.NomeProduto = prod.NomeProduto;
                    produtoAntigo.Descricao = prod.Descricao;
                    produtoAntigo.CategoriaID = prod.CategoriaID;
                    produtoAntigo.Valor = prod.Valor;
                    produtoAntigo.Marca = prod.Marca;
                    
                    //produtoAntigo.CaminhoImagem = prod.CaminhoImagem;

                    contexto.Entry<Produto>(produtoAntigo).State = EntityState.Modified;
                }
                else
                {
                    contexto.Produtos.Add(prod);
                }

                contexto.SaveChanges();
                return true;
            }
        }

        public static Produto CarregarProdutoPorId(int id)
        {
            using (var contexto = new ProdutoContexto())
            {
                Produto prod = contexto.Produtos.AsNoTracking().Where(p => p.ProdutoID == id).FirstOrDefault();
                return prod;
            }
        }

        public static IList<Produto> RetornarProdutosPorCategoria(int? idCategoria)
        {
            using (var contexto = new ProdutoContexto())
            {
                IQueryable<Produto> lista = contexto.Produtos.AsNoTracking();

                if (idCategoria.HasValue && idCategoria > 0)
                {
                    lista = lista.Where(p => p.CategoriaID == idCategoria);
                }

                return lista.ToList<Produto>();
            }
        }

        public static void ExcluirProduto(int idProd)
        {
            using (ProdutoContexto contexto = new ProdutoContexto())
            {
                //Produto prod = contexto.Produtos.Where(p => p.ProdutoID == idProd).FirstOrDefault();
                Produto prod = CarregarProdutoPorId(idProd);

                contexto.Entry<Produto>(prod).State = EntityState.Deleted;

                contexto.SaveChanges();
                
            }
        }
    }
}