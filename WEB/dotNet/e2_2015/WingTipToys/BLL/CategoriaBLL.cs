﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WingTipToys.Models;

namespace WingTipToys.BLL
{
    public class CategoriaBLL
    {
        public static IList<Categoria> ListarCategorias()
        {
            using (var contexto = new ProdutoContexto())
            {
                IQueryable<Categoria> lista = contexto.Categorias.AsNoTracking();
                return lista.ToList<Categoria>();
            }
        }

        public static IList<Categoria> RetornarTodasCategorias()
        {
            using (var contexto = new ProdutoContexto())
            {
                IQueryable<Categoria> lista = contexto.Categorias.AsNoTracking();
                return lista.ToList<Categoria>();
            }
        }

        public static bool CadastrarCategoria(Categoria cat)
        {
            using (var contexto = new ProdutoContexto())
            {
                contexto.Categorias.Add(cat);
                contexto.SaveChanges();

                return true;
            }
        }
    }
}