﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="CadastroProduto.aspx.cs" Inherits="WingTipToys.CadastroProduto" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <h1>Cadastro de Produto</h1>
    <br />
    <table class="table">
        <tr>
            <td class="modal-sm" style="width: 151px; height: 4px">
                <asp:Label ID="lblNome" runat="server" Text="Nome:"></asp:Label>
            </td>
            <td style="height: 4px">
                <asp:TextBox ID="txtNome" CssClass="form-control" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="modal-sm" style="width: 151px">
                <asp:Label ID="lblCategoria" runat="server" Text="Categoria:"></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="ddlCategoria" CssClass="form-control" runat="server"></asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="modal-sm" style="width: 151px">
                <asp:Label ID="lblDescricao" runat="server" Text="Descrição:"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtDescricao" CssClass="form-control" runat="server" TextMode="MultiLine" Rows="3"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="modal-sm" style="width: 151px">
                <asp:Label ID="lblValor" runat="server" Text="Valor:"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtValor" CssClass="form-control" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="modal-sm" style="width: 151px">
                <asp:Button ID="btnAdicionar" CssClass="btn btn-primary" runat="server" Text="Adicionar" OnClick="btnAdicionar_Click" />
            </td>
            <td>
                <asp:Button ID="btnCancelar" CssClass="btn btn-default" runat="server" Text="Cancelar" OnClick="btnCancelar_Click" />
            </td>
        </tr>
    </table>
</asp:Content>
