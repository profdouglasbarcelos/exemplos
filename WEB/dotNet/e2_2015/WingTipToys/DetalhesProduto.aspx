﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="DetalhesProduto.aspx.cs" Inherits="WingTipToys.DetalhesProduto" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <asp:Label ID="lblConfirmarExclusao" runat="server" Text="Tem certeza que deseja excluir o produto a seguir?" Visible="false"></asp:Label>
    <asp:Button ID="btnExcluir" runat="server" Text="Sim" Visible="false" OnClick="btnExcluir_Click" />
    <asp:FormView ID="productDetail" runat="server" ItemType="WingTipToys.Models.Produto" SelectMethod ="CarregarProduto" RenderOuterTable="false" >
        <ItemTemplate>
            <div>
                <h1><%#:Item.NomeProduto %></h1>
            </div>
            <br />
            <table>
                <tr>
                    <td>
                        <img src="/Catalog/Images/<%#:Item.CaminhoImagem %>" style="border:solid; height:300px" alt="<%#:Item.NomeProduto %>"/>
                    </td>
                    <td>&nbsp;</td>  
                    <td style="vertical-align: top; text-align:left;">
                        <b>Descrição:</b><br /><%#:Item.Descricao %><br /><span><b>Preço:</b>&nbsp;<%#: String.Format("{0:c}", Item.Valor) %></span><br /><span><b>Código:</b>&nbsp;<%#:Item.ProdutoID %></span><br /><a href="CadastroProduto.aspx?idProduto=<%#: Item.ProdutoID %>">Editar
                        </a>
                        <a href="DetalhesProduto.aspx?produtoID=<%#: Item.ProdutoID %>&acao=Excluir">
                        Excluir
                        </a>
                    </td>
                </tr>
            </table>
        </ItemTemplate>
    </asp:FormView>


</asp:Content>
