﻿<%@ Page Title="Bem vindo" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="WingTipToys._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <h1><%: Title %></h1>
    <h2>Wingtip Toys pode ajudar você a encontrar um presente perfeito.</h2>
    <p class="lead">
        Nós temos todos os tipos de brinquedos relacionados a transportes.
        Você pode solicitar qualquer um de nossos brinquedos hoje.
        Cada um dos brinquedos tem uma lista detalhada das informações sobre o 
        produto, para que você possa escolher o brinquedo correto. 
    </p>

</asp:Content>
