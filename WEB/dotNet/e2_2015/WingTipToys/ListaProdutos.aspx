﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ListaProdutos.aspx.cs" Inherits="WingTipToys.ListaProdutos" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <section>
        <div>
            <hgroup>
                <h2><%: Page.Title %></h2>
            </hgroup>

            <asp:ListView ID="lvProdutos" runat="server"
                DataKeyNames="ProdutoID" GroupItemCount="4"
                ItemType="WingTipToys.Models.Produto" SelectMethod="RetornarProdutosPorCategoria">
                <EmptyDataTemplate>
                    <table>
                        <tr>
                            <td>Nenhum produto.</td>
                        </tr>
                    </table>
                </EmptyDataTemplate>
                <EmptyItemTemplate>
                    <td />
                </EmptyItemTemplate>
                <GroupTemplate>
                    <tr id="itemPlaceholderContainer" runat="server">
                        <td id="itemPlaceholder" runat="server"></td>
                    </tr>
                </GroupTemplate>
                <ItemTemplate>
                    <td runat="server">
                        <table>
                            <tr>
                                <td>
                                    <a href="DetalhesProduto.aspx?produtoID=<%#:Item.ProdutoID %>">
                                        <img src="/Catalog/Images/Thumbs/<%#:Item.CaminhoImagem %>"
                                            width="100" height="75" style="border: solid" /></a>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <a href="DetalhesProduto.aspx?produtoID=<%#:Item.ProdutoID %>">
                                        <span>
                                            <%#:Item.NomeProduto %>
                                        </span>
                                    </a>
                                    <br />
                                    <span>
                                        <b>Preço: </b><%#:String.Format("{0:c}", Item.Valor)%>
                                    </span>
                                    <br />
                                    <a href="/AdicionarAoCarrinho.aspx?produtoID=<%#:Item.ProdutoID %>">               
                                        <span class="ProductListItem">
                                            <b>Adicionar ao Carrinho<b>
                                        </span>           
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                        </table>
                        </p>
                    </td>
                </ItemTemplate>
                <LayoutTemplate>
                    <table style="width: 100%;">
                        <tbody>
                            <tr>
                                <td>
                                    <table id="groupPlaceholderContainer" runat="server" style="width: 100%">
                                        <tr id="groupPlaceholder"></tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                            </tr>
                            <tr></tr>
                        </tbody>
                    </table>
                </LayoutTemplate>
            </asp:ListView>
        </div>
    </section>
</asp:Content>
