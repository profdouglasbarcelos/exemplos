﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.ModelBinding;
using System.Web.UI;
using System.Web.UI.WebControls;
using WingTipToys.BLL;
using WingTipToys.Models;

namespace WingTipToys
{
    public partial class ListaProdutos : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        //public IQueryable<Produto> ListarProdutos([QueryString("id")] int? idCategoria)
        //{
        //    var contexto = new ProdutoContexto();

        //    IQueryable<Produto> lista = contexto.Produtos.AsNoTracking();

        //    if(idCategoria.HasValue && idCategoria > 0)
        //    {
        //        lista = lista.Where(p => p.CategoriaID == idCategoria);
        //    }

        //    return lista;
        //}

        public IList<Produto> RetornarProdutosPorCategoria([QueryString("id")] int? idCategoria)
        {
            return ProdutoBLL.RetornarProdutosPorCategoria(idCategoria);
        }
    }
}